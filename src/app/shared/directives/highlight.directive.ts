import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  @Input() appHighlight: string = ''
  @HostBinding('style.background-color') get bg() {
    switch (this.appHighlight) {
      case 'danger': return 'red';
      default:
        return 'yellow'
    }
  }

}

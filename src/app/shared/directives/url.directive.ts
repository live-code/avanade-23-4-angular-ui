import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[url]'
})
export class UrlDirective {
  @Input() url: string | undefined;

  @HostListener('click')
  clickMe() {
    if (this.url) {
      window.open(this.url)
    } else {
      throw new Error('you should set an url')
    }
  }
}

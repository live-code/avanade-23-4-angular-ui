import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: 'div[alert]'
})
export class AlertDirective {
  @Input() alert: 'danger' | 'success' | 'info' | '' = 'info'

  @HostBinding() get class() {
    return `alert alert-${this.alert || 'info'}`
  }

}

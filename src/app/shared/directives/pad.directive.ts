import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appPad]'
})
export class PadDirective {
  @Input() appPad: 'lg' | '' | undefined

  @HostBinding() class = 'p-4'
  @HostBinding('style.fontSize') get fontSize() {
    return this.appPad === 'lg' ? '40px' : '20px'
  }


}

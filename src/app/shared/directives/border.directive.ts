import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appBorder]'
})
export class BorderDirective {
  @Input() size: number = 10;

  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) { }

  ngOnChanges() {
    this.renderer.setStyle(
      this.el.nativeElement,
      'border',
      `${this.size}px dashed blue`
    )
  }

}

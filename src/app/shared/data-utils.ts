export class DataUtils<T> {
  private _data: T[] = [];

  add(value: T) {
    this._data.push(value)
  }

  get data(): T[] {
    return this._data
  }
}

/**
 *   const container = new DataUtils<string>();
 *     container.add('pippo')
 *     container.add('pluto')
 *     container.add('topolino')
 *
 *     console.log(container.data[0].)
 *
 *     const container2 = new DataUtils<number>();
 *     container2.add(10)
 *     container2.add(20)
 *     container2.add(30)
 *
 *     console.log(container2.data[0].)
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-hello-lifecycle',
  template: `
    
    <div class="bg-info m-1 p-2">
      <p
        [style.color]="color"
      >hello lifecycle {{name}}</p>
      
      <button 
        *ngIf="labelBtn"
        (click)="buttonClick.emit()"
      >
        {{labelBtn}}
      </button>
    </div>
  `,
})
export class HelloLifecycleComponent {
  @Input() name: string = 'Mario'
  @Input() color: string = 'black'
  @Input() labelBtn = ''
  @Output() buttonClick = new EventEmitter();
}

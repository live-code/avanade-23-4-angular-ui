import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-panel',
  template: `
   
    <div class="card">
      <div
        class="card-header"
        (click)="toggle.emit()"
      >{{title}}</div>
      <div class="card-body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class PanelComponent {
  @Input() title: string | undefined;
  @Input() isOpen = false;
  @Output() toggle = new EventEmitter();
}

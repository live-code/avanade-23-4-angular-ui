import { Component, ElementRef, Input, SimpleChanges, ViewChild } from '@angular/core';
import { LatLngExpression } from 'leaflet';
import * as L from 'leaflet';

@Component({
  selector: 'app-leaflet',
  template: `
    <div #host class="map"></div>
  `,
  styles: [`
    .map { height: 180px; }
  `]
})
export class LeafletComponent {
  @ViewChild('host') host!: ElementRef<HTMLDivElement>
  @Input() coords!: LatLngExpression
  @Input() zoom = 6;
  map!: L.Map;

  ngOnChanges(changes: SimpleChanges) {
    if (this.map) {
      if(changes['zoom']) {
        this.map.setZoom(this.zoom)
      }
      if(changes['coords']) {
        this.map.setView(this.coords)
      }
    }
  }

  ngAfterViewInit() {
    this.map = L.map(this.host.nativeElement)
      .setView(this.coords, this.zoom);

    // marker

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
    }).addTo(this.map);
  }

}

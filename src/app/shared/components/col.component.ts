import { Component, HostBinding, Input } from '@angular/core';
import { RowComponent } from './row.component';

@Component({
  selector: 'app-col',
  template: `
      <ng-content></ng-content>
  `,
  styles: [
  ]
})
export class ColComponent {
  @HostBinding() get class() {
    return 'col-' + this.parent.mq;
  }

  constructor(private parent: RowComponent) {
  }
}

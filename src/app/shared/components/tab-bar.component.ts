import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-tab-bar',
  template: `
  
    <ul class="nav nav-tabs">
      <li 
        class="nav-item"
        *ngFor="let item of items"
        (click)="tabClick.emit(item)"
      >
        <a 
          class="nav-link"
          [ngClass]="{active: item.id === active?.id}"
        >
          {{item[labelField]}}
          <i *ngIf="tabIcon" [class]="tabIcon" 
             (click)="iconClickHandler($event, item)"></i>
        
        </a>
        
      </li>
    </ul>
  `,
})
export class TabBarComponent<T extends { id: number, [key: string]: any }> {
  @Input() items: T[] = [];
  @Input() active: T | null = null;
  @Input() labelField: string = 'name';
  @Input() tabIcon: string | undefined
  @Output() iconClick = new EventEmitter<T>();
  @Output() tabClick = new EventEmitter<T>();

  iconClickHandler(e: MouseEvent, item: T) {
    e.stopPropagation();
    this.iconClick.emit(item)

  }
}



import { HttpClient } from '@angular/common/http';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { delay } from 'rxjs';
import { Meteo } from '../../model/meteo';

@Component({
  selector: 'app-weather',
  template: `
    <h1>Meteo {{city}}</h1>
    <div *ngIf="data">
      Temp: {{data.main.temp}} °

      <img
        [src]="'https://openweathermap.org/img/w/'+ data.weather[0].icon + '.png'" [alt]="'icon meteo ' + city">
    </div>
  `,
})
export class WeatherComponent implements OnChanges {
  @Input() city: string | undefined = '';
  @Input() unit: 'metric' | 'imperial' = 'metric'
  data: Meteo | null = null;

  constructor(private http: HttpClient) {}

  ngOnChanges() {
    if (this.city) {
      this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=${this.unit}&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .pipe(delay(1000))
        .subscribe(res => {
          this.data = res;
        })
    }
  }

}

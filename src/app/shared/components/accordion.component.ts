import { AfterContentInit, Component, ContentChildren, Input, QueryList, ViewChildren } from '@angular/core';
import { PanelComponent } from './panel.component';

@Component({
  selector: 'app-accordion',
  template: `
   <div style="border: 2px solid black">
     <ng-content></ng-content>
   </div>
  `,
})
export class AccordionComponent implements AfterContentInit {
  @Input() multiple = false;
  @ContentChildren(PanelComponent) panels!: QueryList<PanelComponent>;

  ngAfterContentInit() {
    const panels = this.panels.toArray();
    panels.forEach(p => {
      p.isOpen = false;
      p.toggle.subscribe(() => {
        if (this.multiple) {
          p.isOpen = !p.isOpen
        } else {
          this.closeAll();
          p.isOpen = true;
        }

      })
    })
    panels[0].isOpen = true;
  }

  closeAll() {
    const panels = this.panels.toArray();
    panels.forEach(p => p.isOpen = false)
  }
}

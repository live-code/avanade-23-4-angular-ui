import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-separator',
  template: `
    <div 
      class="separator"
      [style.border-color]="color"
      [style.border-style]="type" 
      [ngClass]="{
        'md': margin === 'md',
        'lg': margin === 'lg'
      }"
    >
    </div>
  `,
  styles: [`
    .separator {
      border-width: 1px;
      border-color: black;
      border-style: solid;
      margin: 0;
      height: 1px;
    }
    
    .md {
      margin: 20px 0; 
    }
    .lg {
      margin: 40px 0;
    }
  
  `]
})
export class SeparatorComponent {
  @Input() color: string = 'black';
  @Input() type: 'solid' | 'dotted' | 'dashed' = 'solid';
  @Input() margin: 'sm' | 'md' | 'lg' = 'sm'
}

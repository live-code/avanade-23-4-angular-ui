import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelloLifecycleComponent } from './components/hello-lifecycle.component';
import { WeatherComponent } from './components/weather.component';
import { SeparatorComponent } from './components/separator.component';
import { TabBarComponent } from './components/tab-bar.component';
import { RowComponent } from './components/row.component';
import { ColComponent } from './components/col.component';
import { PanelComponent } from './components/panel.component';
import { AccordionComponent } from './components/accordion.component';
import { LeafletComponent } from './components/leaflet.component';
import { PadDirective } from './directives/pad.directive';
import { AlertDirective } from './directives/alert.directive';
import { BorderDirective } from './directives/border.directive';
import { MarginDirective } from './directives/margin.directive';
import { HighlightDirective } from './directives/highlight.directive';
import { UrlDirective } from './directives/url.directive';
import { StopPropagationDirective } from './directives/stop-propagation.directive';

@NgModule({
  declarations: [
    HelloLifecycleComponent,
    WeatherComponent,
    SeparatorComponent,
    TabBarComponent,
    RowComponent,
    ColComponent,
    PanelComponent,
    AccordionComponent,
    LeafletComponent,
    PadDirective,
    AlertDirective,
    BorderDirective,
    MarginDirective,
    HighlightDirective,
    UrlDirective,
    StopPropagationDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    HelloLifecycleComponent,
    WeatherComponent,
    SeparatorComponent,
    TabBarComponent,
    RowComponent,
    ColComponent,
    PanelComponent,
    AccordionComponent,
    LeafletComponent,
    PadDirective,
    AlertDirective,
    BorderDirective,
    MarginDirective,
    HighlightDirective,
    UrlDirective,
    StopPropagationDirective
  ]
})
export class SharedModule { }

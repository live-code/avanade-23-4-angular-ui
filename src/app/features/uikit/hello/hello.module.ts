import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';

import { HelloRoutingModule } from './hello-routing.module';
import { HelloComponent } from './hello.component';


@NgModule({
  declarations: [
    HelloComponent
  ],
  imports: [
    CommonModule,
    HelloRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class HelloModule { }

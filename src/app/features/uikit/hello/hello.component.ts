import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounce, debounceTime, distinctUntilChanged } from 'rxjs';

@Component({
  selector: 'app-hello',
  template: `
    <h1>Weather</h1>
    
    <input #inputRef type="text" placeholder="Search City" [formControl]="inputSearch">
    
    <app-weather 
      [city]="city"
      [unit]="unit"
    ></app-weather>
    
    <button (click)="city = 'Trieste'">Trieste</button>
    <button (click)="city = 'Rome'">Roma</button>
    <button (click)="city = 'Palermo'">Palermo</button>
   <hr>
    <button (click)="unit = 'metric'">Metric</button>
    <button (click)="unit = 'imperial'">Imperial</button>
    <button (click)="dosomething()">do nothing</button>
    
    <app-separator margin="lg" type="dashed"></app-separator>
    
    <h1>Hello Lifecycle</h1>
    <app-hello-lifecycle
      name="Fabio"
      color="red"
      labelBtn="Visit website"
      (buttonClick)="gotoWebsite('https://fabiobiondi.dev')"
    ></app-hello-lifecycle>

    <app-hello-lifecycle
      name="Mario"
      color="green"
    ></app-hello-lifecycle>

    <app-separator margin="lg" type="dashed"></app-separator>
    
  `,
})
export class HelloComponent implements AfterViewInit, OnDestroy  {
  @ViewChild('inputRef') inputSearchRef!: ElementRef<HTMLInputElement>;

  inputSearch = new FormControl('', { nonNullable: true });
  city: string | undefined;
  unit: 'metric' | 'imperial' = 'metric'

  constructor() {
    this.inputSearch.valueChanges
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(text => {
        this.city = text;
      })
  }

  ngOnChanges() {

  }
  ngOnInit() {
    console.log(this.inputSearchRef)
  }

  ngAfterViewInit() {
    this.inputSearchRef.nativeElement.focus();
  }


  gotoWebsite(url: string) {
    window.open(url)
  }

  showAlert() {

  }

  dosomething() {

  }

  giveFocus(input: HTMLInputElement) {
    input.focus();
  }

  ngOnDestroy() {
    console.log('destroy')
  }


}

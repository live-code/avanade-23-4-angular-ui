import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Misc2RoutingModule } from './misc2-routing.module';
import { Misc2Component } from './misc2.component';


@NgModule({
  declarations: [
    Misc2Component
  ],
  imports: [
    CommonModule,
    Misc2RoutingModule
  ]
})
export class Misc2Module { }

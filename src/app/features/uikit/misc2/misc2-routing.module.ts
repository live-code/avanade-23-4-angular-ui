import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Misc2Component } from './misc2.component';

const routes: Routes = [{ path: '', component: Misc2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Misc2RoutingModule { }

import { Component } from '@angular/core';

@Component({
  selector: 'app-misc2',
  template: `
    <p>misc2 works!</p>
    
    <ul>
      <ng-container  *ngFor="let item of list">
        <li *ngIf="item">
          {{item}}
        </li>
      </ng-container>
    </ul>
    <ng-container *ngIf="data">
      <div>one</div>
      <div>two</div>
      <div>three</div>
    </ng-container>
    
    
    <div *ngIf="data === 123; else empty">ci sono dati: {{data}}</div>
    
    <ng-template #empty>
      niente dati!!!!
    </ng-template>
    
    <!--TODO: show template dinamically-->
    <button onClick></button>
    
  `,
  styles: [`
    ul>li { background-color: red}
  `]
})
export class Misc2Component {
  data = 1253;

  list = ['a', 'b', null, 'c']
}

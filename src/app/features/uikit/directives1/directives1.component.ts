import { Component } from '@angular/core';

@Component({
  selector: 'app-directives1',
  template: `
    
    <h1>URL directive</h1>
    
    <button url>Visit</button>
    <button url="http://www.google.com">Visit</button>
    
    <h1>Highlight directive</h1>
    
    <p>
      Lorem ipsum <span appHighlight>dolor</span> sit amet,
      <span appHighlight="danger">consectetur</span> adipisicing elit. A aliquam beatae blanditiis commodi, delectus deleniti dicta distinctio dolore numquam officiis optio perspiciatis quos reiciendis rem, repellendus sit soluta sunt, unde!
    </p>
    
    <h1 [appMargin]="value">Margin Directive</h1>
    <h1 appBorder [size]="value">Border Directive</h1>
    
    <h1>Alert directive</h1>
    <div alert="success" appPad>content</div>
    <div alert="danger" appPad>content</div>
    <div alert appPad>content</div>
    
    <h1>Pad Directive</h1>
    <div appPad>ciao 1</div>
    <div [appPad]>ciao 2</div>
    <div [appPad]="'lg'">ciao 3 lG</div>
    
  `,
  styles: [
  ]
})
export class Directives1Component {
  value = 3;

  constructor() {
    setInterval(() => {
      if (this.value < 20) {
        this.value+=3;
      }
    }, 1000)

  }
}



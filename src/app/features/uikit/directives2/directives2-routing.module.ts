import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Directives2Component } from './directives2.component';

const routes: Routes = [{ path: '', component: Directives2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Directives2RoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';

import { Directives2RoutingModule } from './directives2-routing.module';
import { Directives2Component } from './directives2.component';


@NgModule({
  declarations: [
    Directives2Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    Directives2RoutingModule
  ]
})
export class Directives2Module { }

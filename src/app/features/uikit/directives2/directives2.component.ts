import { Component } from '@angular/core';

@Component({
  selector: 'app-directives2',
  template: `
    <p>
      directives2 works!
    </p>
   


    <div class="bg-info p-3" (click)="parent()">
      PARENT

      <div stopPropagation (click)="child()" class="bg-danger">
        CHILD
      </div>
    </div>
  `,
  styles: [
  ]
})
export class Directives2Component {

  parent() {
    console.log('parent')
  }

  child() {
    console.log('child')
  }
}

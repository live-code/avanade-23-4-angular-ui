import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { PanelComponent } from '../../../shared/components/panel.component';

@Component({
  selector: 'app-panels',
  template: `
    <div class="container">
      <p>
        panels works!
      </p>

  
      <app-accordion [multiple]="true">
        <app-panel title="1">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. A eligendi eum, ipsam nam odit quisquam quo veritatis voluptatum? Ad, amet doloremque doloribus dolorum facere harum laboriosam maxime nisi qui voluptas.
        </app-panel>
        
        <app-panel title="2">
          <button>Click me</button>
        </app-panel>
        <app-panel title="3">
          <button>Click me</button>
        </app-panel>
      </app-accordion>
    </div>
    
  `,
})
export class PanelsComponent {

}

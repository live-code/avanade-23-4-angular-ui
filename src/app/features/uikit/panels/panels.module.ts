import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';

import { PanelsRoutingModule } from './panels-routing.module';
import { PanelsComponent } from './panels.component';


@NgModule({
  declarations: [
    PanelsComponent
  ],
  imports: [
    CommonModule,
    PanelsRoutingModule,
    SharedModule
  ]
})
export class PanelsModule { }

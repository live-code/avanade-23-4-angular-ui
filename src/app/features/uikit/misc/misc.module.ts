import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';

import { MiscRoutingModule } from './misc-routing.module';
import { MiscComponent } from './misc.component';


@NgModule({
  declarations: [
    MiscComponent
  ],
  imports: [
    CommonModule,
    MiscRoutingModule,
    SharedModule
  ]
})
export class MiscModule { }

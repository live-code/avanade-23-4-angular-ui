import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { City, Country } from '../../../model/country';
import { DataUtils } from '../../../shared/data-utils';

@Component({
  selector: 'app-misc',
  template: `
    
      <h1>Misc Components</h1>
  
      <div class="row">
        <div class="col-sm">left</div>
        <div class="col-sm">center</div>
        <div class="col-sm">right</div>
      </div>
      
      <app-separator margin="md"></app-separator>
      
      <h1>Row 2</h1>
      <app-row mq="sm">
        <app-col><em>left</em></app-col>
        <app-col>center</app-col>
        <app-col>right</app-col>
      </app-row>
    
      <h1>Separators</h1>
      <app-separator></app-separator>
      
      <app-separator
        color="red"
        type="dashed"
        margin="lg"
      ></app-separator>
      
      <app-separator margin="md"></app-separator>
      
      <app-separator></app-separator>
  
  
      <app-tab-bar
        [items]="countries"
        [active]="selectedCountry"
        (tabClick)="setActiveCountry($event)"
        tabIcon="fa fa-trash"
        (iconClick)="removeCountry($event)"
      ></app-tab-bar>
      
      <app-tab-bar
        *ngIf="selectedCountry"
        labelField="label"
        [items]="selectedCountry.cities"
        [active]="selectedCity"
        tabIcon="fa fa-info-circle"
        (iconClick)="gotoWikipedia($event)"
        (tabClick)="setActiveCity($event)"
      >
      </app-tab-bar>
      
      <p>{{selectedCity?.coords | json}}</p>
      <img
        *ngIf="selectedCity"
        width="100%"
        [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=' + selectedCity.label + '&size=1200,800&zoom=7'" alt=""
      >
  `,
})
export class MiscComponent {
  countries: Country[] = [];
  selectedCountry: Country | null = null;
  selectedCity: City | null = null;

  constructor() {
    setTimeout(() => {
      this.countries = [
        {
          id: 11,
          name: 'Italy',
          desc: 'bla bla italy',
          cities: [
            { id: 1001, label: 'Milan', coords: { lat: 41, lng: 12 } },
            { id: 1002, label: 'Rome', coords: { lat: 42, lng: 13 } } ,
            { id: 1003, label: 'Milan', coords: { lat: 43, lng: 14 } }
          ]

        },
        {
          id: 2,
          name: 'Germany',
          desc: 'bla bla deutsch',
          cities: [
            { id: 1001, label: 'Berlino', coords: { lat: 55, lng: 33 } },
          ]
        },
        {
          id: 3,
          name: 'UK',
          desc: 'bla bla english',
          cities: [
            { id: 1004, label: 'London', coords: { lat: 66, lng: 22 } },
            { id: 1007, label: 'Leeds', coords: { lat: 66, lng: 22 } },
          ]
        },
      ];
      this.selectedCountry = this.countries[0];
      this.selectedCity = this.selectedCountry.cities[0];
    }, 500)
  }

  setActiveCountry(country: Country) {
    this.selectedCountry = country;
    this.selectedCity = this.selectedCountry.cities[0];
  }

  setActiveCity(city: City) {
    this.selectedCity = city;
  }

  gotoWikipedia(country: City) {
    window.open(`https://it.wikipedia.org/wiki/${country.label}` )
  }

  removeCountry(country: Country) {
    const index = this.countries.findIndex(c => c.id === country.id)
    this.countries.splice(index, 1);
    if (country.id === this.selectedCountry?.id) {
      this.selectedCountry = null;
      this.selectedCity = null;
    }

  }
}

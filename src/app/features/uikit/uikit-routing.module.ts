import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UikitComponent } from './uikit.component';

const routes: Routes = [
  {
    path: '',
    component: UikitComponent,
    children: [
      { path: 'hello', loadChildren: () => import('./hello/hello.module').then(m => m.HelloModule) },
      { path: 'maps', loadChildren: () => import('./maps/maps.module').then(m => m.MapsModule) },
      { path: 'panels', loadChildren: () => import('./panels/panels.module').then(m => m.PanelsModule) },
      { path: 'misc', loadChildren: () => import('./misc/misc.module').then(m => m.MiscModule) },
      { path: 'directives1', loadChildren: () => import('./directives1/directives1.module').then(m => m.Directives1Module) },
      { path: 'directives2', loadChildren: () => import('./directives2/directives2.module').then(m => m.Directives2Module) },
      { path: 'misc2', loadChildren: () => import('./misc2/misc2.module').then(m => m.Misc2Module) },
      { path: '', redirectTo: 'hello', pathMatch: 'full'},
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UikitRoutingModule { }

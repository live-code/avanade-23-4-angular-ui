import { Component } from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'app-maps',
  template: `
    
    
    <app-leaflet
      [coords]="coords"
      [zoom]="zoom"
    ></app-leaflet>

    <hr>
    <button (click)="zoom = zoom - 1">-</button>
    <button (click)="zoom = zoom + 1">+</button>
    <hr>
    <button (click)="coords={ lat: 44, lng: 11}">Location 1</button>
    <button (click)="coords={ lat: 45, lng: 13}">Location 2</button>
  `,

})
export class MapsComponent {
  zoom = 10;
  coords: any = {
    lat: 11,
    lng: 12
  }

}

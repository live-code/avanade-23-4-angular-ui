import { Component } from '@angular/core';

@Component({
  selector: 'app-uikit',
  template: `
    <button routerLink="hello" routerLinkActive="bg-dark text-white">hello</button>
    <button routerLink="maps" routerLinkActive="bg-dark text-white">maps</button>
    <button routerLink="panels" routerLinkActive="bg-dark text-white">panels</button>
    <button routerLink="misc" routerLinkActive="bg-dark text-white">misc</button>
    <button routerLink="misc2" routerLinkActive="bg-dark text-white">misc2</button>
    <button routerLink="directives1" routerLinkActive="bg-dark text-white">directives1</button>
    <button routerLink="directives2" routerLinkActive="bg-dark text-white">directives2</button>
    
    <hr>
    
    <router-outlet></router-outlet>
  `,
  styles: [
  ]
})
export class UikitComponent {

}

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
   <button routerLink="home" routerLinkActive="bg-dark text-white">Home</button>
   <button routerLink="uikit" routerLinkActive="bg-dark text-white">Uikit Demo</button>
   
   <hr>
   <router-outlet></router-outlet>
   
  `,
  styles: []
})
export class AppComponent {
}

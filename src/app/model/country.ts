export interface Country {
  id: number;
  name: string;
  desc: string;
  cities: City[];
}

export interface City {
  id: number;
  label: string;
  coords: Coords;
}

export interface Coords  {
  lat: number,
  lng: number
}
